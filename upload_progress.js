/**
 * @file
 * Javascript functions for progress bar status on node creation forms
 * 
 * @author Patrick Fournier <patrick at patrickfournier dot com>
 * @author Fabio Varesano <fvaresano at yahoo dot it>
*/

/**
 * Hide the node form and show the busy div
*/
Drupal.upload_progress_hide_timeout = function() {
  var delay = Drupal.settings["upload_progress"]["delay"];
  setTimeout('upload_progress_hide()', delay*1000);
}

function upload_progress_hide() {
  $('#node-form').hide();
  $("#sending").show();
  $("#upload_progress_cancel_link").click(Drupal.upload_progress_show);
}

Drupal.upload_progress_show = function() {
  $('#node-form').show();
  $("#sending").hide();
  
  // "reload" the form
  window.location = window.location;
}

/**
 * Attaches the upload behaviour to the video upload form.
 */
Drupal.upload_progress = function() {
  $('#node-form').submit(Drupal.upload_progress_hide_timeout);
}

// Global killswitch
if (Drupal.jsEnabled) {
  $(document).ready(Drupal.upload_progress);
}
