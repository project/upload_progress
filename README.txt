********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Upload Progress Module
Author: Patrick Fournier <pfournier at whiskyechobravo dot com>
Drupal: 5
********************************************************************
PREREQUISITES:
 None

********************************************************************
DESCRIPTION:

This module allows you to add a progress bar to any node creation
or edit form. You can set the delay before the progress bar appears.

This can be usefull if your node contains video fields, or any
other fields that takes time to process.

********************************************************************
INSTALLATION:

1. Place the entire upload_progress directory into your Drupal 
   modules directory (normally sites/all/modules or modules).

2. Enable the upload_progress module by navigating to:

     Administer > Site building > Modules

3. Give permission to add progress bar to forms there:

     Administer > User management > Access Control

4. Configure the module there:

     Administer > Site configuration > Upload Progress
   
********************************************************************
